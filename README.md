# k8s-projeto2-app

Projeto responsável por aplicar alguns procedimentos e recursos do Kubernetes ensinados no Bootcamp:

- Comandos Básicos do Kubectl;
- Manifestos do Kubernetes como services, deployments;
- Criação e upload de imagens Docker;
- Criação de Pipeline e geração de um Artefato como Imagem de Container;
- Automação de Implantação num cluster Kubernetes;
- Conceitos gerais de Arquitetura de Software.
